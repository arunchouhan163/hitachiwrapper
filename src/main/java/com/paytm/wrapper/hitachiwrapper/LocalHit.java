package com.paytm.wrapper.hitachiwrapper;

import org.springframework.cloud.openfeign.FeignClient;

import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

@FeignClient(name="hitachi-service",configuration=Hitachiconfiguration.class, url="https://bo3-ite.paytm.in")
public interface LocalHit {
	
	@RequestLine("GET /api/v1/terminal/merchant/{mid}")
	@Headers("x-client-token: {client}")
	public String getTerminal(@Param("client")String client,
			@Param("mid")String mid);
	
	@RequestLine("POST /api/v1/terminal")
	@Headers({"Content-Type: application/json"
			, "x-client-token: {client}"})
	@Body(" %7B\"serialNo\":\"{serialNo}\","
			+ "\"mid\":\"{mid}\",\"modelName\":\"{modelNo}\","
			+ "\"monthlyRental\":\"700.0\","
			+ "\"address\": %7B \"address1\":\"Paytm f1\",\"address2\":\"\","
			+ "\"address3\":\"Sec 6\",\"state\":\"Uttar Pradesh\","
			+ "\"city\":\"Gautam Buddha Nagar\",\"country\":\"India\",\"pin\":201301,\"coordinates\":"
			+ "%7B\"latitude\":\"0\",\"longitude\":\"77.319115\"%7D %7D,\"vendorName\":\"PAX\"%7D")
	public String createTerminal(@Param("client")String client,
			@Param("serialNo")String serialNo,
			@Param("mid")String mid,
			@Param("modelNo")String moodelNo);
	
}
