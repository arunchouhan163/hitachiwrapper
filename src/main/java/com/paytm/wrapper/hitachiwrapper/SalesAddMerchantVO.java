
package com.paytm.wrapper.hitachiwrapper;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

/**
 * The Class SalesAddMerchantVO.
 */
public class SalesAddMerchantVO implements Serializable {


	private static final long serialVersionUID = 4598390538391310157L;
	private Date validDateFrom;
	private Date validDateTill;
	private Timestamp valTimeFrom;
	private Timestamp valTimeTo;
	private String businessName;
	private String masterLoginName;
	private String merchantName;
	private String currency;
	private String approvalStatus;
	private String password;
	private String generatedType;
	private String mid;
	private String ocpVal;
	private String status;
	private String reason;
	private Long errorCode;
	private String midGenerateType;
	private String inputVariableName;
	private boolean isApiCreated;
	private String isGUIDEnabled;
	private String autoCreateEnabled;
	private boolean autoCreate;
	private String merchUniqRefEnabled;
	private boolean merchUniqRef;
	private boolean guid;
	private boolean addMoneyEnable;
	private boolean hybridTxnEnable;
	private boolean refundToBankEnabled;
	private String refundToBank;
	private Long entityId;
	private boolean checkEnabledLater;
	private boolean ocp;
	private String storeCardDetails;
	private String validFrom;
	private String validTo;
	private String midGeneration;
	private String customName;
	private String mobileNumber;
	private String phoneNumber;
	private String email;
	private String address1;
	private String address2;
	private String address3;
	private String country;
	private String state;
	private String city;
	private Long pin;
	private String systemGenerated;
	private String custom;
	private String multiSupport;
	private String pEmail;
	private int howMany;
	private String requestName;
	private String addMoney;
	private String hybridTxn;
	private String requestType;
	private String id;
	private List<String> request;
	private long parentEntityID;
	private String errorMsg;
	private int numberOfRetry;
	private boolean encryptRequestResponse;
	private String encReqRes;
	private String checkLater;
	private String checkSum;
	private String storeCard;
	private boolean checksumEnabled;
	private boolean peonEnabled;
	private boolean callbackURLEnabled;
	private String peonUrl;
	private int peonEnableInt;
	private int callbackURLEnabledInt;
	private int accReq;
	private String ProfileId;
	private boolean walletRefund;
	private int walletRefundInt;
	private boolean walletRecharge;
	private int walletRechargeInt;
	private boolean emailAlert;
	private int emailAlertInt;
	private String walletRechargeOpt;
	private Long aggregatorId;
	private boolean convenienceFeeEnable;
	private String convenienceFee;
	private boolean aggregatorEnable;
	private Integer aggregator;
	private Integer convenienceFeeType;
	private boolean withOutConvenienceFeeEnable;
	private String withOutConvenienceFee;
	private String commSettings;
	private String commStatSelect;
	private boolean emailMerchant;
	private boolean emailConsumer;
	private boolean smsMerchant;
	private boolean smsConsumer;
	private int[] commId;
	private boolean moreParamsInRespEnabled;
	private String moreParamsInRespPreference;
	private String viewDynamicQROnCashierEnabled;
	private boolean viewDynamicQROnCashier;
	private boolean dynamicQRwith2FA;
	private String dynamicQRwith2FAEnabled;
	private boolean onlineSettlement;
	private String onlineSettlementEnabled;
	
	private boolean enableCashierPage;
	private String enableCashierPageEnabled;
	
	private boolean s2sRefund;
	private String s2sRefundEnabled;
	private String s2sRefundUrl;
	private boolean isS2SRefundUrlUpdated;

	private String ffPeonUrl;
	private boolean ffPeonUrlUpdated;
	
	private String merchantType;


	private boolean cancelAllowed;
	private String cancelAllowedEnabled;
	private Integer cancelAllowTime;
	
	
	private Boolean apiDisable;
	private String apiDisabled;
	private Boolean raEnable;
	private String raEnabled;
	private Boolean lobEnable;
	private String lobEnabled;
	private boolean mockMerchant;
	private String mockEnable;

	// secondary user details
	private String sFirstName;
	private String sLastName;
	private String sMobileNumber;
	private String sPhoneNumber;
	private String sEmail;

	// communication business address
	private Boolean sameAsBusinessAddr;
	private String communicationAddress1;
	private String communicationAddress2;
	private String communicationAddress3;
	private String communicationCountry;
	private String communicationState;
	private String communicationCity;
	private Long communicationPin;

	// refund notification
	private boolean refEmailMerchant;
	private boolean refEmailConsumer;
	private boolean refSmsMerchant;
	private boolean refSmsConsumer;
	private String refCommStatSelect;
	private String refundCommSettings;
	private int[] refCommId;

	// KYC Details of Business:
	private String kycBusinessPanNo;
	private String kycBankAccountNo;
	private String kycBusinessIFSCNo;
	private String kycBankAccountHolderName;
	private String kycBankName;
	private String kycBusinessGSTIN;


	// KYC Details of Authorized Signatory:
	private String kycAuthorizedSignatoryName;
	private String kycAuthorizedSignatoryPanNo;
	private String kycAuthorizedSignatoryProofNo;
	private String kycAuthorizedSignatoryIdProofNo;
	private String category;
	private String subCategory;

	// wallet specific fields
	private boolean offlineEnabled;
	private boolean walletOnlyEnabled;
	private String offlineMerchant;
	private String walletOnlyMerchant;
	private String ssoId;
	private Long postConvThresHold;
	private String van;
	
	private Boolean QuickEdit;
	private Boolean ump;
	private String panelLoginName;
	private boolean prnValidation;
	private String prnValidationPref;
	private Integer prnExpiryTime;
	private Integer maxPrnValidRetryAllowTime;
	private Integer maxPrnValidRetryAllowCount;
	private boolean qrprn;
	private String apiVersion;
	private boolean sdMerchant;

	// BW config
	private boolean bwEnabled;
	private String bwEnabledValue;
	private String bwTransferMode;
	private boolean bwAutoTransfer;
	private String bwTriggerMode;
	private String bwTriggerValue;
	private boolean flagMerchant;
	private String flagMerchantValue;

	private String partialRenewalAllowedEnabled;
	private boolean partialRenewalAllowed;
	private Integer minPartialPercentage;

	// auto debit
	private String autoDebitEnabled;
	private boolean autoDebit;
	private String agreementContractNumber;
	private Integer agreementConfirmTimeout;
	private String unlinkNotifyUrl;
	private Boolean unlinkNeedAgreement;
	private Boolean supportInitOtp;
		
	
	public SalesAddMerchantVO(){
		
	}
	
	public SalesAddMerchantVO(SalesAddMerchantVO merchantVO){
		this.convenienceFeeType = merchantVO.getConvenienceFeeType();
		this.requestType = merchantVO.getRequestType();
		this.numberOfRetry = merchantVO.getNumberOfRetry();
		this.refundToBankEnabled = merchantVO.isRefundToBankEnabled();
		this.walletOnlyEnabled = merchantVO.getWalletOnlyEnabled();
		this.request = merchantVO.getRequest();
	}
	
	
	
	//field for PGPlus
	private Integer settlementDays;
	private boolean islinkedBased;
	
	//merchant down-time notifications
	private boolean merchNotificationOnEmail;
	private boolean merchNotificationOnSms;
	private String merchantNotificationEmail;
	private String merchantNotificationMobile;
	private String merchNotifiedEMail;
	private String merchNotifiedSms;
	
	public boolean isMerchNotificationOnEmail() {
		return merchNotificationOnEmail;
	}

	public void setMerchNotificationOnEmail(boolean merchNotificationOnEmail) {
		this.merchNotificationOnEmail = merchNotificationOnEmail;
	}

	public boolean isMerchNotificationOnSms() {
		return merchNotificationOnSms;
	}

	public boolean isFlagMerchant() {
		return flagMerchant;
	}

	public void setFlagMerchant(boolean flagMerchant) {
		this.flagMerchant = flagMerchant;
	}


	public void setMerchNotificationOnSms(boolean merchNotificationOnSms) {
		this.merchNotificationOnSms = merchNotificationOnSms;
	}

	public String getMerchantNotificationEmail() {
		return merchantNotificationEmail;
	}

	public void setMerchantNotificationEmail(String merchantNotificationEmail) {
		this.merchantNotificationEmail = merchantNotificationEmail;
	}

	public String getMerchantNotificationMobile() {
		return merchantNotificationMobile;
	}

	public void setMerchantNotificationMobile(String merchantNotificationMobile) {
		this.merchantNotificationMobile = merchantNotificationMobile;
	}

	public String getMerchNotifiedEMail() {
		return merchNotifiedEMail;
	}

	public void setMerchNotifiedEMail(String merchNotifiedEMail) {
		this.merchNotifiedEMail = merchNotifiedEMail;
	}

	public String getMerchNotifiedSms() {
		return merchNotifiedSms;
	}

	public void setMerchNotifiedSms(String merchNotifiedSms) {
		this.merchNotifiedSms = merchNotifiedSms;
	}

	public Integer getSettlementDays() {
		return settlementDays;
	}

	public void setSettlementDays(Integer settlementDays) {
		this.settlementDays = settlementDays;
	}

	public Boolean getQuickEdit() {
		return QuickEdit;
	}

	public void setQuickEdit(Boolean quickEdit) {
		QuickEdit = quickEdit;
	}

	//Refund Destinations
	private boolean refundToSource;
	private boolean refundToBalance;

	public boolean isRefundToBalance() {
		return refundToBalance;
	}

	public void setRefundToBalance(boolean refundToBalance) {
		this.refundToBalance = refundToBalance;
	}

	private String webSiteName[];
	private String input[]; // added to get the input param
	private String requestUrl[];
	private String responseUrl[];
	private String peonURL[];
	private String imageName[];
	private List<MultipartFile> imageFile0;
	private List<MultipartFile> imageFile1;
	private List<MultipartFile> imageFile2;
	private List<MultipartFile> imageFile3;
	private List<MultipartFile> imageFile4;
	private List<MultipartFile> imageFile5;
	private List<MultipartFile> imageFile6;
	private List<MultipartFile> imageFile7;
	private List<MultipartFile> imageFile8;
	private List<MultipartFile> imageFile9;
	private MultipartFile file;
	private String[] detailedList;
    private String accountFor;
    private String accountPrimary;
    private String canEditPMobile;
    private String buttonValue;
    private String  type;
    private boolean onboardQRMerchant=false;
	private boolean s2sNotifiedEnabled;
	private String s2sNotified;
	private Integer orderExpiryTime;
 	private boolean subUsrFlg;
	private boolean pendingToSuccessRefundEnabled;
	private Long pendingToSuccessRefundTime;
	private String pendingToSuccessRefundPreference;
	private String sapCode;
	private boolean topupSubwallet;
	private boolean automateGratification;
	private boolean automateBCWallet;
	private String subwalletTopup;
	private String gratificationAutomate;
	private String BCWalletAutomate;
	private String requestId;
	private String industryType;
	private String kybID;
	private String ppiLimit;
	private boolean refundRevoke;
	private String refundDisable;
	private String billingPos;
	private String merchantQrTag;
	private boolean enableQRTag;
	
	private MultipartFile logoFile;
	private String logoAWSURL;

	//adding fields for wallet integration
	private String payoutTimeOfTheDay;
	private Integer payoutAfterDays;
	private Integer utsExpiry;
	private String logoUrl;
	private String timeSlot;
	private String offlineMerchantCallbackUrl;
	private String callbackRequestParams;
	private String serviceParams;
	private String walletRequestType;
	private Short callbackServiceType;
	
   private Boolean pushNotification;
   private Boolean withdrawOTPEnable;
   private Boolean exemptServiceTax;
   private Boolean withdrawPushEnable;
   private Boolean pccEnabled;
   private Boolean autoSweep;
   private Boolean offlinePostConvenience;
   private Boolean insertContentType;
   private Boolean customizedCallback;
   private Boolean wInvoiceEnabled;
   
	private String pushNotificationValue;
	private String withdrawOTPEnableValue;
	private String exemptServiceTaxValue;
	private String withdrawPushEnableValue;
	private String pccEnabledValue;
	private String autoSweepValue;
	private String offlinePostConvenienceValue;
	private String insertContentTypeValue;
	private String customizedCallbackValue;
	private String wInvoiceEnabledValue;
	
	private boolean allowanceWalletConfigured;
	
	private MultipartFile foodWalletMidsFile;
	private MultipartFile foodWalletCategoriesFile;
	private boolean rollBack;
	private String rollBackValue;
	private String foodWalletMids;
	private String foodWalletCategories;
	private String dailyTxnAmount;
	private boolean editCall;
	private boolean foodWalletConfigured;
	private boolean foodFileEdited;
	
	public boolean isFoodFileEdited() {
		return foodFileEdited;
	}
	public void setFoodFileEdited(boolean foodFileEdited) {
		this.foodFileEdited = foodFileEdited;
	}
	public boolean isAllowanceWalletConfigured() {
		return allowanceWalletConfigured;
	}
	public void setAllowanceWalletConfigured(boolean allowanceWalletConfigured) {
		this.allowanceWalletConfigured = allowanceWalletConfigured;
	}
	public MultipartFile getFoodWalletMidsFile() {
		return foodWalletMidsFile;
	}

	public void setFoodWalletMidsFile(MultipartFile foodWalletMidsFile) {
		this.foodWalletMidsFile = foodWalletMidsFile;
	}

	public MultipartFile getFoodWalletCategoriesFile() {
		return foodWalletCategoriesFile;
	}

	public void setFoodWalletCategoriesFile(MultipartFile foodWalletCategoriesFile) {
		this.foodWalletCategoriesFile = foodWalletCategoriesFile;
	}

	public boolean isRollBack() {
		return rollBack;
	}

	public void setRollBack(boolean rollBack) {
		this.rollBack = rollBack;
	}

	public String getRollBackValue() {
		return rollBackValue;
	}

	public void setRollBackValue(String rollBackValue) {
		this.rollBackValue = rollBackValue;
	}	
	
	
	public Boolean getwInvoiceEnabled() {
		return wInvoiceEnabled;
	}

	public void setwInvoiceEnabled(Boolean wInvoiceEnabled) {
		this.wInvoiceEnabled = wInvoiceEnabled;
	}

	public String getwInvoiceEnabledValue() {
		return wInvoiceEnabledValue;
	}

	public void setwInvoiceEnabledValue(String wInvoiceEnabledValue) {
		this.wInvoiceEnabledValue = wInvoiceEnabledValue;
	}

	public String getPayoutTimeOfTheDay() {
		return payoutTimeOfTheDay;
	}

	public void setPayoutTimeOfTheDay(String payoutTimeOfTheDay) {
		this.payoutTimeOfTheDay = payoutTimeOfTheDay;
	}

	public Integer getPayoutAfterDays() {
		return payoutAfterDays;
	}

	public void setPayoutAfterDays(Integer payoutAfterDays) {
		this.payoutAfterDays = payoutAfterDays;
	}

	public Integer getUtsExpiry() {
		return utsExpiry;
	}

	public void setUtsExpiry(Integer utsExpiry) {
		this.utsExpiry = utsExpiry;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}

	public String getOfflineMerchantCallbackUrl() {
		return offlineMerchantCallbackUrl;
	}

	public void setOfflineMerchantCallbackUrl(String offlineMerchantCallbackUrl) {
		this.offlineMerchantCallbackUrl = offlineMerchantCallbackUrl;
	}

	public String getCallbackRequestParams() {
		return callbackRequestParams;
	}

	public void setCallbackRequestParams(String callbackRequestParams) {
		this.callbackRequestParams = callbackRequestParams;
	}

	public String getServiceParams() {
		return serviceParams;
	}

	public void setServiceParams(String serviceParams) {
		this.serviceParams = serviceParams;
	}

	public String getWalletRequestType() {
		return walletRequestType;
	}

	public void setWalletRequestType(String walletRequestType) {
		this.walletRequestType = walletRequestType;
	}

	public Short getCallbackServiceType() {
		return callbackServiceType;
	}

	public void setCallbackServiceType(Short callbackServiceType) {
		this.callbackServiceType = callbackServiceType;
	}

	public Boolean getPushNotification() {
		return pushNotification;
	}

	public void setPushNotification(Boolean pushNotification) {
		this.pushNotification = pushNotification;
	}

	public Boolean getWithdrawOTPEnable() {
		return withdrawOTPEnable;
	}

	public void setWithdrawOTPEnable(Boolean withdrawOTPEnable) {
		this.withdrawOTPEnable = withdrawOTPEnable;
	}

	public Boolean getExemptServiceTax() {
		return exemptServiceTax;
	}

	public void setExemptServiceTax(Boolean exemptServiceTax) {
		this.exemptServiceTax = exemptServiceTax;
	}

	public Boolean getWithdrawPushEnable() {
		return withdrawPushEnable;
	}

	public void setWithdrawPushEnable(Boolean withdrawPushEnable) {
		this.withdrawPushEnable = withdrawPushEnable;
	}

	public Boolean getPccEnabled() {
		return pccEnabled;
	}

	public void setPccEnabled(Boolean pccEnabled) {
		this.pccEnabled = pccEnabled;
	}

	public Boolean getAutoSweep() {
		return autoSweep;
	}

	public void setAutoSweep(Boolean autoSweep) {
		this.autoSweep = autoSweep;
	}

	public Boolean getOfflinePostConvenience() {
		return offlinePostConvenience;
	}

	public void setOfflinePostConvenience(Boolean offlinePostConvenience) {
		this.offlinePostConvenience = offlinePostConvenience;
	}

	public Boolean getInsertContentType() {
		return insertContentType;
	}

	public void setInsertContentType(Boolean insertContentType) {
		this.insertContentType = insertContentType;
	}

	public Boolean getCustomizedCallback() {
		return customizedCallback;
	}

	public void setCustomizedCallback(Boolean customizedCallback) {
		this.customizedCallback = customizedCallback;
	}

	public String getPushNotificationValue() {
		return pushNotificationValue;
	}

	public void setPushNotificationValue(String pushNotificationValue) {
		this.pushNotificationValue = pushNotificationValue;
	}

	public String getWithdrawOTPEnableValue() {
		return withdrawOTPEnableValue;
	}

	public void setWithdrawOTPEnableValue(String withdrawOTPEnableValue) {
		this.withdrawOTPEnableValue = withdrawOTPEnableValue;
	}

	public String getExemptServiceTaxValue() {
		return exemptServiceTaxValue;
	}

	public void setExemptServiceTaxValue(String exemptServiceTaxValue) {
		this.exemptServiceTaxValue = exemptServiceTaxValue;
	}

	public String getWithdrawPushEnableValue() {
		return withdrawPushEnableValue;
	}

	public void setWithdrawPushEnableValue(String withdrawPushEnableValue) {
		this.withdrawPushEnableValue = withdrawPushEnableValue;
	}

	public String getPccEnabledValue() {
		return pccEnabledValue;
	}

	public void setPccEnabledValue(String pccEnabledValue) {
		this.pccEnabledValue = pccEnabledValue;
	}

	public String getAutoSweepValue() {
		return autoSweepValue;
	}

	public void setAutoSweepValue(String autoSweepValue) {
		this.autoSweepValue = autoSweepValue;
	}

	public String getOfflinePostConvenienceValue() {
		return offlinePostConvenienceValue;
	}

	public void setOfflinePostConvenienceValue(String offlinePostConvenienceValue) {
		this.offlinePostConvenienceValue = offlinePostConvenienceValue;
	}

	public String getInsertContentTypeValue() {
		return insertContentTypeValue;
	}

	public void setInsertContentTypeValue(String insertContentTypeValue) {
		this.insertContentTypeValue = insertContentTypeValue;
	}

	public String getCustomizedCallbackValue() {
		return customizedCallbackValue;
	}

	public void setCustomizedCallbackValue(String customizedCallbackValue) {
		this.customizedCallbackValue = customizedCallbackValue;
	}
	
	public MultipartFile getLogoFile() {
		return logoFile;
	}

	public void setLogoFile(MultipartFile logoFile) {
		this.logoFile = logoFile;
	}

	public String getLogoAWSURL() {
		return logoAWSURL;
	}

	public void setLogoAWSURL(String logoAWSURL) {
		this.logoAWSURL = logoAWSURL;
	}

	public boolean isRefundToSource() {
		return refundToSource;
	}

	public void setRefundToSource(boolean refundToSource) {
		this.refundToSource = refundToSource;
	}

	
	
	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getKybID() {
		return kybID;
	}

	public void setKybID(String kybID) {
		this.kybID = kybID;
	}

	public String getPpiLimit() {
		return ppiLimit;
	}

	public void setPpiLimit(String ppiLimit) {
		this.ppiLimit = ppiLimit;
	}


	public String getSubwalletTopup() {
		return subwalletTopup;
	}
	public void setSubwalletTopup(String subwalletTopup) {
		this.subwalletTopup = subwalletTopup;
	}
	public String getGratificationAutomate() {
		return gratificationAutomate;
	}
	public void setGratificationAutomate(String gratificationAutomate) {
		this.gratificationAutomate = gratificationAutomate;
	}
	public String getBCWalletAutomate() {
		return BCWalletAutomate;
	}
	public void setBCWalletAutomate(String bCWalletAutomate) {
		BCWalletAutomate = bCWalletAutomate;
	}
	public String getSapCode() {
		return sapCode;
	}
	public void setSapCode(String sapCode) {
		this.sapCode = sapCode;
	}
	public boolean isTopupSubwallet() {
		return topupSubwallet;
	}
	public void setTopupSubwallet(boolean topupSubwallet) {
		this.topupSubwallet = topupSubwallet;
	}
	public boolean isAutomateGratification() {
		return automateGratification;
	}
	public void setAutomateGratification(boolean automateGratification) {
		this.automateGratification = automateGratification;
	}
	public boolean isAutomateBCWallet() {
		return automateBCWallet;
	}
	public void setAutomateBCWallet(boolean automateBCWallet) {
		this.automateBCWallet = automateBCWallet;
	}
	public boolean isOnboardQRMerchant() {
		return onboardQRMerchant;
	}
	public void setOnboardQRMerchant(boolean onboardQRMerchant) {
		this.onboardQRMerchant = onboardQRMerchant;
	}

	public String getMerchUniqRefEnabled() {
		return merchUniqRefEnabled;
	}

	public void setMerchUniqRefEnabled(String merchUniqRefEnabled) {
		this.merchUniqRefEnabled = merchUniqRefEnabled;
	}

	public boolean isMerchUniqRef() {
		return merchUniqRef;
	}

	public void setMerchUniqRef(boolean merchUniqRef) {
		this.merchUniqRef = merchUniqRef;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getButtonValue() {
		return buttonValue;
	}

	public void setButtonValue(String buttonValue) {
		this.buttonValue = buttonValue;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	private Boolean isCommission;
	private String commissionType;
	// invoice Email
	private String invoiceEmail;

	public Boolean getIsCommission() {
		return isCommission;
	}

	public void setIsCommission(Boolean isCommission) {
		this.isCommission = isCommission;
	}

	public String getCommissionType() {
		return commissionType;
	}

	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}

	public String getRefundCommSettings() {
		return refundCommSettings;
	}

	public void setRefundCommSettings(String refundCommSettings) {
		this.refundCommSettings = refundCommSettings;
	}

	public String getsFirstName() {
		return sFirstName;
	}

	public void setsFirstName(String sFirstName) {
		this.sFirstName = sFirstName;
	}

	public String getsLastName() {
		return sLastName;
	}

	public void setsLastName(String sLastName) {
		this.sLastName = sLastName;
	}


	public String getsMobileNumber() {
		return sMobileNumber;
	}

	public void setsMobileNumber(String sMobileNumber) {
		this.sMobileNumber = sMobileNumber;
	}

	public String getsPhoneNumber() {
		return sPhoneNumber;
	}

	public void setsPhoneNumber(String sPhoneNumber) {
		this.sPhoneNumber = sPhoneNumber;
	}


	public int[] getCommId() {
		return commId;
	}

	public void setCommId(int[] commId) {
		this.commId = commId;
	}

	public String getCommStatSelect() {
		return commStatSelect;
	}

	public void setCommStatSelect(String commStatSelect) {
		this.commStatSelect = commStatSelect;
	}

	public boolean isEmailMerchant() {
		return emailMerchant;
	}

	public void setEmailMerchant(boolean emailMerchant) {
		this.emailMerchant = emailMerchant;
	}

	public boolean isEmailConsumer() {
		return emailConsumer;
	}

	public void setEmailConsumer(boolean emailConsumer) {
		this.emailConsumer = emailConsumer;
	}

	public boolean isSmsMerchant() {
		return smsMerchant;
	}

	public void setSmsMerchant(boolean smsMerchant) {
		this.smsMerchant = smsMerchant;
	}

	public boolean isSmsConsumer() {
		return smsConsumer;
	}

	public void setSmsConsumer(boolean smsConsumer) {
		this.smsConsumer = smsConsumer;
	}

	public String getCommSettings() {
		return commSettings;
	}

	public void setCommSettings(String commSettings) {
		this.commSettings = commSettings;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getRefundToBank() {
		return refundToBank;
	}

	public void setRefundToBank(String refundToBank) {
		this.refundToBank = refundToBank;
	}

	public boolean isRefundToBankEnabled() {
		return refundToBankEnabled;
	}

	public void setRefundToBankEnabled(boolean refundToBankEnabled) {
		this.refundToBankEnabled = refundToBankEnabled;
	}

	public long getParentEntityID() {
		return parentEntityID;
	}

	public void setParentEntityID(long parentEntityID) {
		this.parentEntityID = parentEntityID;
	}

	public boolean isAddMoneyEnable() {
		return addMoneyEnable;
	}

	public void setAddMoneyEnable(boolean addMoneyEnable) {
		this.addMoneyEnable = addMoneyEnable;
	}

	public boolean isHybridTxnEnable() {
		return hybridTxnEnable;
	}

	public void setHybridTxnEnable(boolean hybridTxnEnable) {
		this.hybridTxnEnable = hybridTxnEnable;
	}

	public int getCallbackURLEnabledInt() {
		return callbackURLEnabledInt;
	}

	public void setCallbackURLEnabledInt(int callbackURLEnabledInt) {
		this.callbackURLEnabledInt = callbackURLEnabledInt;
	}

	public String getAddMoney() {
		return addMoney;
	}

	public String getHybridTxn() {
		return hybridTxn;
	}

	public List<String> getRequest() {
		return request;
	}

	public void setRequest(List<String> request) {
		this.request = request;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public Long getErrorCode() {
		return errorCode;
	}

	public int getNumberOfRetry() {
		return numberOfRetry;
	}

	public void setNumberOfRetry(int numberOfRetry) {
		this.numberOfRetry = numberOfRetry;
	}

	private int sizeOfKey;


	public int getSizeOfKey() {
		return sizeOfKey;
	}

	public void setSizeOfKey(int sizeOfKey) {
		this.sizeOfKey = sizeOfKey;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the error msg.
	 *
	 * @return the error msg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * Sets the error msg.
	 *
	 * @param errorMsg the new error msg
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	/**
	 * Gets the mid.
	 *
	 * @return the mid
	 */
	public String getMid() {
		return mid;
	}

	/**
	 * Sets the mid.
	 *
	 * @param mid the new mid
	 */
	public void setMid(String mid) {
		this.mid = mid;
	}

	/**
	 * Gets the approval status.
	 *
	 * @return the approval status
	 */
	public String getApprovalStatus() {
		return approvalStatus;
	}

	/**
	 * Sets the approval status.
	 *
	 * @param approvalStatus the new approval status
	 */
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfileId() {
		return ProfileId;
	}

	public void setProfileId(String profileId) {
		ProfileId = profileId;
	}

	public boolean isWalletRefund() {
		return walletRefund;
	}

	public void setWalletRefund(boolean walletRefund) {
		this.walletRefund = walletRefund;
	}

	public int getWalletRefundInt() {
		return walletRefundInt;
	}

	public void setWalletRefundInt(int walletRefundInt) {
		this.walletRefundInt = walletRefundInt;
	}

	public boolean isWalletRecharge() {
		return walletRecharge;
	}

	public void setWalletRecharge(boolean walletRecharge) {
		this.walletRecharge = walletRecharge;
	}

	public int getWalletRechargeInt() {
		return walletRechargeInt;
	}

	public void setWalletRechargeInt(int walletRechargeInt) {
		this.walletRechargeInt = walletRechargeInt;
	}


	public boolean isEmailAlert() {
		return emailAlert;
	}

	public void setEmailAlert(boolean emailAlert) {
		this.emailAlert = emailAlert;
	}


	public int getEmailAlertInt() {
		return emailAlertInt;
	}

	public void setEmailAlertInt(int emailAlertInt) {
		this.emailAlertInt = emailAlertInt;
	}

	public String getWalletRechargeOpt() {
		return walletRechargeOpt;
	}

	public void setWalletRechargeOpt(String walletRechargeOpt) {
		this.walletRechargeOpt = walletRechargeOpt;
	}

	public int getAccReq() {
		return accReq;
	}

	public void setAccReq(int accReq) {
		this.accReq = accReq;
	}

	public boolean isPeonEnabled() {
		return peonEnabled;
	}

	public void setPeonEnabled(boolean peonEnabled) {
		this.peonEnabled = peonEnabled;
	}

	public String getPeonUrl() {
		return peonUrl;
	}

	public void setPeonUrl(String peonUrl) {
		this.peonUrl = peonUrl;
	}

	public int getPeonEnableInt() {
		return peonEnableInt;
	}

	public void setPeonEnableInt(int peonEnableInt) {
		this.peonEnableInt = peonEnableInt;
	}


	public boolean isCallbackURLEnabled() {
		return callbackURLEnabled;
	}

	public void setCallbackURLEnabled(boolean callbackURLEnabled) {
		this.callbackURLEnabled = callbackURLEnabled;
	}

	/**
	 * Gets the enc req res.
	 *
	 * @return the enc req res
	 */
	public String getEncReqRes() {
		return encReqRes;
	}

	/**
	 * Sets the enc req res.
	 *
	 * @param encReqRes the new enc req res
	 */
	public void setEncReqRes(String encReqRes) {
		this.encReqRes = encReqRes;
	}

	/**
	 * Gets the check later.
	 *
	 * @return the check later
	 */
	public String getCheckLater() {
		return checkLater;
	}

	/**
	 * Sets the check later.
	 *
	 * @param checkLater the new check later
	 */
	public void setCheckLater(String checkLater) {
		this.checkLater = checkLater;
	}

	/**
	 * Gets the check sum.
	 *
	 * @return the check sum
	 */
	public String getCheckSum() {
		return checkSum;
	}

	/**
	 * Sets the check sum.
	 *
	 * @param checkSum the new check sum
	 */
	public void setCheckSum(String checkSum) {
		this.checkSum = checkSum;
	}

	/**
	 * Gets the store card.
	 *
	 * @return the store card
	 */
	public String getStoreCard() {
		return storeCard;
	}

	/**
	 * Sets the store card.
	 *
	 * @param storeCard the new store card
	 */
	public void setStoreCard(String storeCard) {
		this.storeCard = storeCard;
	}

	/**
	 * Gets the p email.
	 *
	 * @return the p email
	 */
	public String getpEmail() {
		return pEmail;
	}

	/**
	 * Sets the p email.
	 *
	 * @param pEmail the new p email
	 */
	public void setpEmail(String pEmail) {
		this.pEmail = pEmail;
	}

	/**
	 * Gets the s email.
	 *
	 * @return the s email
	 */
	public String getsEmail() {
		return sEmail;
	}

	/**
	 * Sets the s email.
	 *
	 * @param sEmail the new s email
	 */
	public void setsEmail(String sEmail) {
		this.sEmail = sEmail;
	}

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/** The user name. */
	private String userName;

	/**
	 * Gets the business name.
	 *
	 * @return the business name
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * Sets the business name.
	 *
	 * @param businessName the new business name
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * Gets the merchant name.
	 *
	 * @return the merchant name
	 */
	public String getMerchantName() {
		return merchantName;
	}

	/**
	 * Sets the merchant name.
	 *
	 * @param merchantName the new merchant name
	 */
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * Gets the encrypt request response.
	 *
	 * @return the encrypt request response
	 */
	public boolean getEncryptRequestResponse() {
		return encryptRequestResponse;
	}

	/**
	 * Sets the encrypt request response.
	 *
	 * @param encryptRequestResponse the new encrypt request response
	 */
	public void setEncryptRequestResponse(boolean encryptRequestResponse) {
		this.encryptRequestResponse = encryptRequestResponse;
	}

	/**
	 * Gets the checksum enabled.
	 *
	 * @return the checksum enabled
	 */
	public boolean getChecksumEnabled() {
		return checksumEnabled;
	}

	/**
	 * Sets the checksum enabled.
	 *
	 * @param checksumEnabled the new checksum enabled
	 */
	public void setChecksumEnabled(boolean checksumEnabled) {
		this.checksumEnabled = checksumEnabled;
	}

	/**
	 * Gets the check enabled later.
	 *
	 * @return the check enabled later
	 */
	public boolean getCheckEnabledLater() {
		return checkEnabledLater;
	}

	/**
	 * Sets the check enabled later.
	 *
	 * @param checkEnabledLater the new check enabled later
	 */
	public void setCheckEnabledLater(boolean checkEnabledLater) {
		this.checkEnabledLater = checkEnabledLater;
	}

	/**
	 * Gets the store card details.
	 *
	 * @return the store card details
	 */
	public String getStoreCardDetails() {
		return storeCardDetails;
	}

	/**
	 * Sets the store card details.
	 *
	 * @param storeCardDetails the new store card details
	 */
	public void setStoreCardDetails(String storeCardDetails) {
		this.storeCardDetails = storeCardDetails;
	}

	/**
	 * Gets the valid from.
	 *
	 * @return the valid from
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * Sets the valid from.
	 *
	 * @param validFrom the new valid from
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * Gets the valid to.
	 *
	 * @return the valid to
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * Sets the valid to.
	 *
	 * @param validTo the new valid to
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * Gets the mid generation.
	 *
	 * @return the mid generation
	 */
	public String getMidGeneration() {
		return midGeneration;
	}

	/**
	 * Sets the mid generation.
	 *
	 * @param midGeneration the new mid generation
	 */
	public void setMidGeneration(String midGeneration) {
		this.midGeneration = midGeneration;
	}

	/**
	 * Gets the custom name.
	 *
	 * @return the custom name
	 */
	public String getCustomName() {
		return customName;
	}

	/**
	 * Sets the custom name.
	 *
	 * @param customName the new custom name
	 */
	public void setCustomName(String customName) {
		this.customName = customName;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber the new mobile number
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber the new phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the address1.
	 *
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * Sets the address1.
	 *
	 * @param address1 the new address1
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * Gets the address2.
	 *
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * Sets the address2.
	 *
	 * @param address2 the new address2
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the pin.
	 *
	 * @return the pin
	 */
	public Long getPin() {
		return pin;
	}

	/**
	 * Sets the pin.
	 *
	 * @param pin the new pin
	 */
	public void setPin(Long pin) {
		this.pin = pin;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Sets the system generated.
	 *
	 * @param systemGenerated the new system generated
	 */
	public void setSystemGenerated(String systemGenerated) {
		this.systemGenerated = systemGenerated;
	}

	/**
	 * Gets the system generated.
	 *
	 * @return the system generated
	 */
	public String getSystemGenerated() {
		return systemGenerated;
	}

	/**
	 * Sets the custom.
	 *
	 * @param custom the new custom
	 */
	public void setCustom(String custom) {
		this.custom = custom;
	}

	/**
	 * Gets the custom.
	 *
	 * @return the custom
	 */
	public String getCustom() {
		return custom;
	}

	/**
	 * Sets the multi support.
	 *
	 * @param multiSupport the new multi support
	 */
	public void setMultiSupport(String multiSupport) {
		this.multiSupport = multiSupport;
	}

	/**
	 * Gets the multi support.
	 *
	 * @return the multi support
	 */
	public String getMultiSupport() {
		return multiSupport;
	}

	/**
	 * Sets the how many.
	 *
	 * @param howMany the new how many
	 */
	public void setHowMany(int howMany) {
		this.howMany = howMany;
	}

	/**
	 * Gets the how many.
	 *
	 * @return the how many
	 */
	public int getHowMany() {
		return howMany;
	}

	/**
	 * Sets the request name.
	 *
	 * @param requestName the new request name
	 */
	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}

	/**
	 * Gets the request name.
	 *
	 * @return the request name
	 */
	public String getRequestName() {
		return requestName;
	}

	/**
	 * Sets the generated type.
	 *
	 * @param generatedType the new generated type
	 */
	public void setGeneratedType(String generatedType) {
		this.generatedType = generatedType;
	}

	/**
	 * Gets the generated type.
	 *
	 * @return the generated type
	 */
	public String getGeneratedType() {
		return generatedType;
	}

	/**
	 * Checks if is ocp.
	 *
	 * @return true, if is ocp
	 */
	public boolean isOcp() {
		return ocp;
	}

	/**
	 * Sets the ocp.
	 *
	 * @param ocp the new ocp
	 */
	public void setOcp(boolean ocp) {
		this.ocp = ocp;
	}

	/**
	 * Sets the master login name.
	 *
	 * @param masterLoginName the new master login name
	 */
	public void setMasterLoginName(String masterLoginName) {
		this.masterLoginName = masterLoginName;
	}

	/**
	 * Gets the master login name.
	 *
	 * @return the master login name
	 */
	public String getMasterLoginName() {
		return masterLoginName;
	}

	/**
	 * Gets the valid date from.
	 *
	 * @return the valid date from
	 */
	public Date getValidDateFrom() {
		return validDateFrom;
	}

	/**
	 * Sets the valid date from.
	 *
	 * @param validDateFrom the new valid date from
	 */
	public void setValidDateFrom(Date validDateFrom) {
		this.validDateFrom = validDateFrom;
	}

	/**
	 * Gets the valid date till.
	 *
	 * @return the valid date till
	 */
	public Date getValidDateTill() {
		return validDateTill;
	}

	/**
	 * Sets the valid date till.
	 *
	 * @param validDateTill the new valid date till
	 */
	public void setValidDateTill(Date validDateTill) {
		this.validDateTill = validDateTill;
	}

	/**
	 * Sets the ocp val.
	 *
	 * @param ocpVal the new ocp val
	 */
	public void setOcpVal(String ocpVal) {
		this.ocpVal = ocpVal;
	}

	/**
	 * Gets the ocp val.
	 *
	 * @return the ocp val
	 */
	public String getOcpVal() {
		return ocpVal;
	}

	/**
	 * Sets the val time from.
	 *
	 * @param valTimeFrom the new val time from
	 */
	public void setValTimeFrom(Timestamp valTimeFrom) {
		this.valTimeFrom = valTimeFrom;
	}

	/**
	 * Gets the val time from.
	 *
	 * @return the val time from
	 */
	public Timestamp getValTimeFrom() {
		return valTimeFrom;
	}

	/**
	 * Sets the val time to.
	 *
	 * @param valTimeTo the new val time to
	 */
	public void setValTimeTo(Timestamp valTimeTo) {
		this.valTimeTo = valTimeTo;
	}

	/**
	 * Gets the val time to.
	 *
	 * @return the val time to
	 */
	public Timestamp getValTimeTo() {
		return valTimeTo;
	}

	/**
	 * Sets the mid generate type.
	 *
	 * @param midGenerateType the new mid generate type
	 */
	public void setMidGenerateType(String midGenerateType) {
		this.midGenerateType = midGenerateType;
	}

	/**
	 * Gets the mid generate type.
	 *
	 * @return the mid generate type
	 */
	public String getMidGenerateType() {
		return midGenerateType;
	}

	/**
	 * Sets the input variable name.
	 *
	 * @param inputVariableName the new input variable name
	 */
	public void setInputVariableName(String inputVariableName) {
		this.inputVariableName = inputVariableName;
	}

	/**
	 * Gets the input variable name.
	 *
	 * @return the input variable name
	 */
	public String getInputVariableName() {
		return inputVariableName;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the reason.
	 *
	 * @param reason the new reason
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * Gets the reason.
	 *
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/** The Profile. */
	private String profile;
	/** The ProfileID. */
	private String profileId;

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getIsGUIDEnabled() {
		return isGUIDEnabled;
	}

	public void setIsGUIDEnabled(String isGUIDEnabled) {
		this.isGUIDEnabled = isGUIDEnabled;
	}

	public boolean isGuid() {
		return guid;
	}

	public void setGuid(boolean guid) {
		this.guid = guid;
	}

	public void setAddMoney(String addMoney) {
		this.addMoney = addMoney;
	}

	public void setHybridTxn(String hybridTxn) {
		this.hybridTxn = hybridTxn;
	}

	public Integer getConvenienceFeeType() {
		return convenienceFeeType;
	}

	public void setConvenienceFeeType(Integer convenienceFeeType) {
		this.convenienceFeeType = convenienceFeeType;
	}

	public boolean isWithOutConvenienceFeeEnable() {
		return withOutConvenienceFeeEnable;
	}

	public void setWithOutConvenienceFeeEnable(boolean withOutConvenienceFeeEnable) {
		this.withOutConvenienceFeeEnable = withOutConvenienceFeeEnable;
	}

	public String getWithOutConvenienceFee() {
		return withOutConvenienceFee;
	}

	public void setWithOutConvenienceFee(String withOutConvenienceFee) {
		this.withOutConvenienceFee = withOutConvenienceFee;
	}

	public boolean isConvenienceFeeEnable() {
		return convenienceFeeEnable;
	}

	public void setConvenienceFeeEnable(boolean convenienceFeeEnable) {
		this.convenienceFeeEnable = convenienceFeeEnable;
	}

	public String getConvenienceFee() {
		return convenienceFee;
	}

	public void setConvenienceFee(String convenienceFee) {
		this.convenienceFee = convenienceFee;
	}

	public Long getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(Long aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public boolean isAggregatorEnable() {
		return aggregatorEnable;
	}

	public void setAggregatorEnable(boolean aggregatorEnable) {
		this.aggregatorEnable = aggregatorEnable;
	}

	public Integer getAggregator() {
		return aggregator;
	}

	public void setAggregator(Integer aggregator) {
		this.aggregator = aggregator;
	}

	public boolean isApiCreated() {
		return isApiCreated;
	}

	public void setApiCreated(boolean isApiCreated) {
		this.isApiCreated = isApiCreated;
	}

	public boolean getAutoCreate() {
		return autoCreate;
	}

	public void setAutoCreate(boolean autCreate) {
		this.autoCreate = autCreate;
	}

	public String getAutoCreateEnabled() {
		return autoCreateEnabled;
	}

	public void setAutoCreateEnabled(String autoCreateEnabled) {
		this.autoCreateEnabled = autoCreateEnabled;
	}

	public String getMoreParamsInRespPreference() {
		return moreParamsInRespPreference;
	}

	public void setMoreParamsInRespPreference(String moreParamsInRespPreference) {
		this.moreParamsInRespPreference = moreParamsInRespPreference;
	}

	public boolean isMoreParamsInRespEnabled() {
		return moreParamsInRespEnabled;
	}

	public void setMoreParamsInRespEnabled(boolean moreParamsInRespEnabled) {
		this.moreParamsInRespEnabled = moreParamsInRespEnabled;
	}

	public Boolean getSameAsBusinessAddr() {
		return sameAsBusinessAddr;
	}

	public void setSameAsBusinessAddr(Boolean sameAsBusinessAddr) {
		this.sameAsBusinessAddr = sameAsBusinessAddr;
	}

	public String getKycBusinessPanNo() {
		return kycBusinessPanNo;
	}

	public void setKycBusinessPanNo(String kycBusinessPanNo) {
		this.kycBusinessPanNo = kycBusinessPanNo;
	}

	public String getKycBankAccountNo() {
		return kycBankAccountNo;
	}

	public void setKycBankAccountNo(String kycBankAccountNo) {
		this.kycBankAccountNo = kycBankAccountNo;
	}

	public String getKycBankAccountHolderName() {
		return kycBankAccountHolderName;
	}

	public void setKycBankAccountHolderName(String kycBankAccountHolderName) {
		this.kycBankAccountHolderName = kycBankAccountHolderName;
	}

	public String getKycBusinessIFSCNo() {
		return kycBusinessIFSCNo;
	}

	public void setKycBusinessIFSCNo(String kycBusinessIFSCNo) {
		this.kycBusinessIFSCNo = kycBusinessIFSCNo;
	}

	public String getKycAuthorizedSignatoryName() {
		return kycAuthorizedSignatoryName;
	}

	public void setKycAuthorizedSignatoryName(String kycAuthorizedSignatoryName) {
		this.kycAuthorizedSignatoryName = kycAuthorizedSignatoryName;
	}

	public String getKycAuthorizedSignatoryPanNo() {
		return kycAuthorizedSignatoryPanNo;
	}

	public void setKycAuthorizedSignatoryPanNo(String kycAuthorizedSignatoryPanNo) {
		this.kycAuthorizedSignatoryPanNo = kycAuthorizedSignatoryPanNo;
	}

	public String getKycAuthorizedSignatoryProofNo() {
		return kycAuthorizedSignatoryProofNo;
	}

	public void setKycAuthorizedSignatoryProofNo(String kycAuthorizedSignatoryProofNo) {
		this.kycAuthorizedSignatoryProofNo = kycAuthorizedSignatoryProofNo;
	}

	public String getKycAuthorizedSignatoryIdProofNo() {
		return kycAuthorizedSignatoryIdProofNo;
	}

	public void setKycAuthorizedSignatoryIdProofNo(String kycAuthorizedSignatoryIdProofNo) {
		this.kycAuthorizedSignatoryIdProofNo = kycAuthorizedSignatoryIdProofNo;
	}

	public String getCommunicationAddress1() {
		return communicationAddress1;
	}

	public void setCommunicationAddress1(String communicationAddress1) {
		this.communicationAddress1 = communicationAddress1;
	}

	public String getCommunicationAddress2() {
		return communicationAddress2;
	}

	public void setCommunicationAddress2(String communicationAddress2) {
		this.communicationAddress2 = communicationAddress2;
	}

	public String getCommunicationCountry() {
		return communicationCountry;
	}

	public void setCommunicationCountry(String communicationCountry) {
		this.communicationCountry = communicationCountry;
	}

	public String getCommunicationState() {
		return communicationState;
	}

	public void setCommunicationState(String communicationState) {
		this.communicationState = communicationState;
	}

	public String getCommunicationCity() {
		return communicationCity;
	}

	public void setCommunicationCity(String communicationCity) {
		this.communicationCity = communicationCity;
	}

	public Long getCommunicationPin() {
		return communicationPin;
	}

	public void setCommunicationPin(Long communicationPin) {
		this.communicationPin = communicationPin;
	}

	public String getInvoiceEmail() {
		return invoiceEmail;
	}

	public void setInvoiceEmail(String invoiceEmail) {
		this.invoiceEmail = invoiceEmail;
	}

	public void setSubCategory(Map<String, String> subCategories) {
		// TODO Auto-generated method stub

	}

	public void setSubCategory(List<String> populateSubCategories) {
		// TODO Auto-generated method stub

	}


	public boolean getOfflineEnabled() {
		return offlineEnabled;
	}

	public void setOfflineEnabled(boolean offlineEnabled) {
		this.offlineEnabled = offlineEnabled;
	}

	public boolean getWalletOnlyEnabled() {
		return walletOnlyEnabled;
	}

	public void setWalletOnlyEnabled(boolean walletOnlyEnabled) {
		this.walletOnlyEnabled = walletOnlyEnabled;
	}

	public String getOfflineMerchant() {
		return offlineMerchant;
	}

	public void setOfflineMerchant(String offlineMerchant) {
		this.offlineMerchant = offlineMerchant;
	}

	public String getWalletOnlyMerchant() {
		return walletOnlyMerchant;
	}

	public void setWalletOnlyMerchant(String walletOnlyMerchant) {
		this.walletOnlyMerchant = walletOnlyMerchant;
	}

	public String getSsoId() {
		return ssoId;
	}

	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}

	public String[] getWebSiteName() {
		return webSiteName;
	}

	public void setWebSiteName(String[] webSiteName) {
		this.webSiteName = webSiteName;
	}

	public String[] getInput() {
		return input;
	}

	public void setInput(String[] input) {
		this.input = input;
	}

	public String[] getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String[] requestUrl) {
		this.requestUrl = requestUrl;
	}

	public String[] getResponseUrl() {
		return responseUrl;
	}

	public void setResponseUrl(String[] responseUrl) {
		this.responseUrl = responseUrl;
	}

	public String[] getPeonURL() {
		return peonURL;
	}

	public void setPeonURL(String[] peonURL) {
		this.peonURL = peonURL;
	}

	public String[] getImageName() {
		return imageName;
	}

	public void setImageName(String[] imageName) {
		this.imageName = imageName;
	}

	public List<MultipartFile> getImageFile0() {
		return imageFile0;
	}

	public void setImageFile0(List<MultipartFile> imageFile0) {
		this.imageFile0 = imageFile0;
	}

	public List<MultipartFile> getImageFile1() {
		return imageFile1;
	}

	public void setImageFile1(List<MultipartFile> imageFile1) {
		this.imageFile1 = imageFile1;
	}

	public List<MultipartFile> getImageFile2() {
		return imageFile2;
	}

	public void setImageFile2(List<MultipartFile> imageFile2) {
		this.imageFile2 = imageFile2;
	}

	public List<MultipartFile> getImageFile3() {
		return imageFile3;
	}

	public void setImageFile3(List<MultipartFile> imageFile3) {
		this.imageFile3 = imageFile3;
	}

	public List<MultipartFile> getImageFile4() {
		return imageFile4;
	}

	public void setImageFile4(List<MultipartFile> imageFile4) {
		this.imageFile4 = imageFile4;
	}

	public List<MultipartFile> getImageFile5() {
		return imageFile5;
	}

	public void setImageFile5(List<MultipartFile> imageFile5) {
		this.imageFile5 = imageFile5;
	}

	public List<MultipartFile> getImageFile6() {
		return imageFile6;
	}

	public void setImageFile6(List<MultipartFile> imageFile6) {
		this.imageFile6 = imageFile6;
	}

	public List<MultipartFile> getImageFile7() {
		return imageFile7;
	}

	public void setImageFile7(List<MultipartFile> imageFile7) {
		this.imageFile7 = imageFile7;
	}

	public List<MultipartFile> getImageFile8() {
		return imageFile8;
	}

	public void setImageFile8(List<MultipartFile> imageFile8) {
		this.imageFile8 = imageFile8;
	}

	public List<MultipartFile> getImageFile9() {
		return imageFile9;
	}

	public void setImageFile9(List<MultipartFile> imageFile9) {
		this.imageFile9 = imageFile9;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String[] getDetailedList() {
		return detailedList;
	}

	public void setDetailedList(String[] detailedList) {
		this.detailedList = detailedList;
	}

	public String getAccountFor() {
		return accountFor;
	}

	public void setAccountFor(String accountFor) {
		this.accountFor = accountFor;
	}

	public String getAccountPrimary() {
		return accountPrimary;
	}

	public void setAccountPrimary(String accountPrimary) {
		this.accountPrimary = accountPrimary;
	}

	public String getCanEditPMobile() {
		return canEditPMobile;
	}

	public void setCanEditPMobile(String canEditPMobile) {
		this.canEditPMobile = canEditPMobile;
	}


	public boolean isSubUsrFlg() {
		return subUsrFlg;
	}

	public void setSubUsrFlg(boolean subUsrFlg) {
		this.subUsrFlg = subUsrFlg;
	}

	public int[] getRefCommId() {
		return refCommId;
	}

	public void setRefCommId(int[] refCommId) {
		this.refCommId = refCommId;
	}

	public boolean isRefEmailMerchant() {
		return refEmailMerchant;
	}

	public void setRefEmailMerchant(boolean refEmailMerchant) {
		this.refEmailMerchant = refEmailMerchant;
	}

	public boolean isRefEmailConsumer() {
		return refEmailConsumer;
	}

	public void setRefEmailConsumer(boolean refEmailConsumer) {
		this.refEmailConsumer = refEmailConsumer;
	}

	public boolean isRefSmsMerchant() {
		return refSmsMerchant;
	}

	public void setRefSmsMerchant(boolean refSmsMerchant) {
		this.refSmsMerchant = refSmsMerchant;
	}

	public boolean isRefSmsConsumer() {
		return refSmsConsumer;
	}

	public void setRefSmsConsumer(boolean refSmsConsumer) {
		this.refSmsConsumer = refSmsConsumer;
	}

	public String getRefCommStatSelect() {
		return refCommStatSelect;
	}

	public void setRefCommStatSelect(String refCommStatSelect) {
		this.refCommStatSelect = refCommStatSelect;
	}
    public boolean getS2sNotifiedEnabled() {
		return s2sNotifiedEnabled;
	}

	public void setS2sNotifiedEnabled(boolean s2sNotifiedEnabled) {
		this.s2sNotifiedEnabled = s2sNotifiedEnabled;
	}

	public Integer getOrderExpiryTime() {
		return orderExpiryTime;
	}

	public void setOrderExpiryTime(Integer orderExpiryTime) {
		this.orderExpiryTime = orderExpiryTime;
	}

	public boolean isPendingToSuccessRefundEnabled() {
		return pendingToSuccessRefundEnabled;
	}

	public void setPendingToSuccessRefundEnabled(boolean pendingToSuccessRefundEnabled) {
		this.pendingToSuccessRefundEnabled = pendingToSuccessRefundEnabled;
	}

	public Long getPendingToSuccessRefundTime() {
		return pendingToSuccessRefundTime;
	}

	public void setPendingToSuccessRefundTime(Long pendingToSuccessRefundTime) {
		this.pendingToSuccessRefundTime = pendingToSuccessRefundTime;
	}

	public String getPendingToSuccessRefundPreference() {
		return pendingToSuccessRefundPreference;
	}

	public void setPendingToSuccessRefundPreference(String pendingToSuccessRefundPreference) {
		this.pendingToSuccessRefundPreference = pendingToSuccessRefundPreference;
	}


	public String getKycBankName() {
		return kycBankName;
	}

	public void setKycBankName(String kycBankName) {
		this.kycBankName = kycBankName;
	}
	

	public Long getPostConvThresHold() {
		return postConvThresHold;
	}

	public void setPostConvThresHold(Long postConvThresHold) {
		this.postConvThresHold = postConvThresHold;
	}
	 public String getS2sNotified() {
			return s2sNotified;
		}

		public void setS2sNotified(String s2sNotified) {
			this.s2sNotified = s2sNotified;
		}
		
		public String getKycBusinessGSTIN() {
			return kycBusinessGSTIN;
		}
		public void setKycBusinessGSTIN(String kycBusinessGSTIN) {
			this.kycBusinessGSTIN = kycBusinessGSTIN;
		}
		public String getRefundDisable() {
			return refundDisable;
		}

		public void setRefundDisable(String refundDisable) {
			this.refundDisable = refundDisable;
		}

		public boolean isRefundRevoke() {
			return refundRevoke;
		}

		public void setRefundRevoke(boolean refundRevoke) {
			this.refundRevoke = refundRevoke;
		}

		public String getBillingPos() {
			return billingPos;
		}

		public void setBillingPos(String billingPos) {
			this.billingPos = billingPos;
		}
		
		public String getAddress3() {
			return address3;
		}

		public void setAddress3(String address3) {
			this.address3 = address3;
		}

		public String getCommunicationAddress3() {
			return communicationAddress3;
		}

		public void setCommunicationAddress3(String communicationAddress3) {
			this.communicationAddress3 = communicationAddress3;
		}

	@Override
	public String toString() {
		return "SalesAddMerchantVO [validDateFrom=" + validDateFrom + ", validDateTill=" + validDateTill + ", valTimeFrom=" + valTimeFrom
				+ ", valTimeTo=" + valTimeTo + ", businessName=" + businessName + ", masterLoginName=" + masterLoginName + ", merchantName="
				+ merchantName + ", currency=" + currency + ", approvalStatus=" + approvalStatus + ", password=" + password + ", generatedType="
				+ generatedType + ", mid=" + mid + ", ocpVal=" + ocpVal + ", status=" + status + ", reason=" + reason + ", errorCode=" + errorCode
				+ ", midGenerateType=" + midGenerateType + ", inputVariableName=" + inputVariableName + ", isApiCreated=" + isApiCreated
				+ ", isGUIDEnabled=" + isGUIDEnabled + ", autoCreateEnabled=" + autoCreateEnabled + ", autoCreate=" + autoCreate
				+ ", merchUniqRefEnabled=" + merchUniqRefEnabled + ", merchUniqRef=" + merchUniqRef + ", guid=" + guid + ", addMoneyEnable="
				+ addMoneyEnable + ", hybridTxnEnable=" + hybridTxnEnable + ", refundToBankEnabled=" + refundToBankEnabled + ", refundToBank="
				+ refundToBank + ", entityId=" + entityId + ", checkEnabledLater=" + checkEnabledLater + ", ocp=" + ocp + ", storeCardDetails="
				+ storeCardDetails + ", validFrom=" + validFrom + ", validTo=" + validTo + ", midGeneration=" + midGeneration + ", customName="
				+ customName + ", mobileNumber=" + mobileNumber + ", phoneNumber=" + phoneNumber + ", email=" + email + ", address1=" + address1
				+ ", address2=" + address2 + ", address3=" + address3 + ", country=" + country + ", state=" + state + ", city=" + city + ", pin=" + pin
				+ ", systemGenerated=" + systemGenerated + ", custom=" + custom + ", multiSupport=" + multiSupport + ", pEmail=" + pEmail + ", howMany="
				+ howMany + ", requestName=" + requestName + ", addMoney=" + addMoney + ", hybridTxn=" + hybridTxn + ", requestType=" + requestType
				+ ", id=" + id + ", request=" + request + ", parentEntityID=" + parentEntityID + ", errorMsg=" + errorMsg + ", numberOfRetry="
				+ numberOfRetry + ", encryptRequestResponse=" + encryptRequestResponse + ", encReqRes=" + encReqRes + ", checkLater=" + checkLater
				+ ", checkSum=" + checkSum + ", storeCard=" + storeCard + ", checksumEnabled=" + checksumEnabled + ", peonEnabled=" + peonEnabled
				+ ", callbackURLEnabled=" + callbackURLEnabled + ", peonUrl=" + peonUrl + ", peonEnableInt=" + peonEnableInt + ", callbackURLEnabledInt="
				+ callbackURLEnabledInt + ", accReq=" + accReq + ", ProfileId=" + ProfileId + ", walletRefund=" + walletRefund + ", walletRefundInt="
				+ walletRefundInt + ", walletRecharge=" + walletRecharge + ", walletRechargeInt=" + walletRechargeInt + ", emailAlert=" + emailAlert
				+ ", emailAlertInt=" + emailAlertInt + ", walletRechargeOpt=" + walletRechargeOpt + ", aggregatorId=" + aggregatorId
				+ ", convenienceFeeEnable=" + convenienceFeeEnable + ", convenienceFee=" + convenienceFee + ", aggregatorEnable=" + aggregatorEnable
				+ ", aggregator=" + aggregator + ", convenienceFeeType=" + convenienceFeeType + ", withOutConvenienceFeeEnable="
				+ withOutConvenienceFeeEnable + ", withOutConvenienceFee=" + withOutConvenienceFee + ", commSettings=" + commSettings
				+ ", commStatSelect=" + commStatSelect + ", emailMerchant=" + emailMerchant + ", emailConsumer=" + emailConsumer + ", smsMerchant="
				+ smsMerchant + ", smsConsumer=" + smsConsumer + ", commId=" + Arrays.toString(commId) + ", moreParamsInRespEnabled="
				+ moreParamsInRespEnabled + ", moreParamsInRespPreference=" + moreParamsInRespPreference + ", viewDynamicQROnCashierEnabled="
				+ viewDynamicQROnCashierEnabled + ", viewDynamicQROnCashier=" + viewDynamicQROnCashier + ", dynamicQRwith2FA=" + dynamicQRwith2FA
				+ ", dynamicQRwith2FAEnabled=" + dynamicQRwith2FAEnabled + ", sFirstName=" + sFirstName + ", sLastName=" + sLastName + ", sMobileNumber="
				+ sMobileNumber + ", sPhoneNumber=" + sPhoneNumber + ", sEmail=" + sEmail + ", sameAsBusinessAddr=" + sameAsBusinessAddr
				+ ", communicationAddress1=" + communicationAddress1 + ", communicationAddress2=" + communicationAddress2 + ", communicationAddress3="
				+ communicationAddress3 + ", communicationCountry=" + communicationCountry + ", communicationState=" + communicationState
				+ ", communicationCity=" + communicationCity + ", communicationPin=" + communicationPin + ", refEmailMerchant=" + refEmailMerchant
				+ ", refEmailConsumer=" + refEmailConsumer + ", refSmsMerchant=" + refSmsMerchant + ", refSmsConsumer=" + refSmsConsumer
				+ ", refCommStatSelect=" + refCommStatSelect + ", refundCommSettings=" + refundCommSettings + ", refCommId=" + Arrays.toString(refCommId)
				+ ", kycBusinessPanNo=" + kycBusinessPanNo + ", kycBankAccountNo=" + kycBankAccountNo + ", kycBusinessIFSCNo=" + kycBusinessIFSCNo
				+ ", kycBankAccountHolderName=" + kycBankAccountHolderName + ", kycBankName=" + kycBankName + ", kycBusinessGSTIN=" + kycBusinessGSTIN
				+ ", kycAuthorizedSignatoryName=" + kycAuthorizedSignatoryName + ", kycAuthorizedSignatoryPanNo=" + kycAuthorizedSignatoryPanNo
				+ ", kycAuthorizedSignatoryProofNo=" + kycAuthorizedSignatoryProofNo + ", kycAuthorizedSignatoryIdProofNo="
				+ kycAuthorizedSignatoryIdProofNo + ", category=" + category + ", subCategory=" + subCategory + ", offlineEnabled=" + offlineEnabled
				+ ", walletOnlyEnabled=" + walletOnlyEnabled + ", offlineMerchant=" + offlineMerchant + ", walletOnlyMerchant=" + walletOnlyMerchant
				+ ", ssoId=" + ssoId + ", postConvThresHold=" + postConvThresHold + ", QuickEdit=" + QuickEdit + ", ump=" + ump + ", settlementDays="
				+ settlementDays + ", refundToSource=" + refundToSource + ", refundToBalance=" + refundToBalance + ", webSiteName="
				+ Arrays.toString(webSiteName) + ", input=" + Arrays.toString(input) + ", requestUrl=" + Arrays.toString(requestUrl) + ", responseUrl="
				+ Arrays.toString(responseUrl) + ", peonURL=" + Arrays.toString(peonURL) + ", imageName=" + Arrays.toString(imageName) + ", imageFile0="
				+ imageFile0 + ", imageFile1=" + imageFile1 + ", imageFile2=" + imageFile2 + ", imageFile3=" + imageFile3 + ", imageFile4=" + imageFile4
				+ ", imageFile5=" + imageFile5 + ", imageFile6=" + imageFile6 + ", imageFile7=" + imageFile7 + ", imageFile8=" + imageFile8
				+ ", imageFile9=" + imageFile9 + ", file=" + file + ", detailedList=" + Arrays.toString(detailedList) + ", accountFor=" + accountFor
				+ ", accountPrimary=" + accountPrimary + ", canEditPMobile=" + canEditPMobile + ", buttonValue=" + buttonValue + ", type=" + type
				+ ", onboardQRMerchant=" + onboardQRMerchant + ", s2sNotifiedEnabled=" + s2sNotifiedEnabled + ", s2sNotified=" + s2sNotified
				+ ", orderExpiryTime=" + orderExpiryTime + ", subUsrFlg=" + subUsrFlg + ", pendingToSuccessRefundEnabled=" + pendingToSuccessRefundEnabled
				+ ", pendingToSuccessRefundTime=" + pendingToSuccessRefundTime + ", pendingToSuccessRefundPreference=" + pendingToSuccessRefundPreference
				+ ", sapCode=" + sapCode + ", topupSubwallet=" + topupSubwallet + ", automateGratification=" + automateGratification
				+ ", automateBCWallet=" + automateBCWallet + ", subwalletTopup=" + subwalletTopup + ", gratificationAutomate=" + gratificationAutomate
				+ ", BCWalletAutomate=" + BCWalletAutomate + ", requestId=" + requestId + ", industryType=" + industryType + ", kybID=" + kybID
				+ ", ppiLimit=" + ppiLimit + ", refundRevoke=" + refundRevoke + ", refundDisable=" + refundDisable + ", billingPos=" + billingPos
				+ ", merchantQrTag=" + merchantQrTag + ", enableQRTag=" + enableQRTag + ", logoFile=" + logoFile + ", logoAWSURL=" + logoAWSURL
				+ ", isCommission=" + isCommission + ", commissionType=" + commissionType + ", invoiceEmail=" + invoiceEmail + ", sizeOfKey=" + sizeOfKey
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", userName=" + userName + ", profile=" + profile + ", profileId=" + profileId
				+ ", islinkedBased=" + islinkedBased + ", panelLoginName=" + panelLoginName + ", foodWalletMidsFile=" + foodWalletMidsFile + ", foodWalletCategoriesFile=" + foodWalletCategoriesFile + ", rollBack=" + rollBack
				+ ", rollBackValue=" + rollBackValue +", onlineSettlement=" + onlineSettlement + ", onlineSettlementEnabled=" + onlineSettlementEnabled
+ ", s2sRefund=" + s2sRefund + ", s2sRefundEnable=" + s2sRefundEnabled + ", s2sRefundUrl=" +s2sRefundUrl + ", ffPeonUrl=" + ffPeonUrl
				+ ", cancelAllowed=" + cancelAllowed + ", cancelAllowedEnabled=" + cancelAllowedEnabled + ", van=" + van + ", sdMerchant=" + sdMerchant +", bwEnabled=" + bwEnabled
				+ ", bwTransferMode=" + bwTransferMode + ", bwAutoTransfer=" + bwAutoTransfer + ", bwTriggerMode=" + bwTriggerMode + ", bwTriggerValue="
				+ bwTriggerValue + ", flagMerchant=" + flagMerchant + ", autoDebit=" + autoDebit + ", unlinkNeedAgreement=" + unlinkNeedAgreement
				+ ", supportInitOtp=" + supportInitOtp + ", agreementContractNumber=" + agreementContractNumber + ", agreementConfirmTimeout="
				+ agreementConfirmTimeout + ", unlinkNotifyUrl=" + unlinkNotifyUrl + "]";
	}
		
	public String getMerchantQrTag() {
		return merchantQrTag;
	}
	
	public void setMerchantQrTag(String merchantQrTag) {
		this.merchantQrTag = merchantQrTag;
	}

	public boolean isEnableQRTag() {
		return enableQRTag;
	}
		
	public void setEnableQRTag(boolean enableQRTag) {
		this.enableQRTag = enableQRTag;
	}

	public Boolean getUmp() {
		return ump;
	}

	public void setUmp(Boolean ump) {
		this.ump = ump;
	}
		
	public boolean isViewDynamicQROnCashier() {
		return viewDynamicQROnCashier;
	}

	public void setViewDynamicQROnCashier(boolean viewDynamicQROnCashier) {
		this.viewDynamicQROnCashier = viewDynamicQROnCashier;
	}

	public String getViewDynamicQROnCashierEnabled() {
		return viewDynamicQROnCashierEnabled;
	}

	public void setViewDynamicQROnCashierEnabled(String viewDynamicQROnCashierEnabled) {
		this.viewDynamicQROnCashierEnabled = viewDynamicQROnCashierEnabled;
	}

	public boolean isDynamicQRwith2FA() {
		return dynamicQRwith2FA;
	}

	public void setDynamicQRwith2FA(boolean dynamicQRwith2FA) {
		this.dynamicQRwith2FA = dynamicQRwith2FA;
	}

	public String getDynamicQRwith2FAEnabled() {
		return dynamicQRwith2FAEnabled;
	}

	public void setDynamicQRwith2FAEnabled(String dynamicQRwith2FAEnabled) {
		this.dynamicQRwith2FAEnabled = dynamicQRwith2FAEnabled;
	}

	public boolean isIslinkedBased() {
		return islinkedBased;
	}

	public void setIslinkedBased(boolean islinkedBased) {
		this.islinkedBased = islinkedBased;
	}
	public String getFoodWalletMids() {
		return foodWalletMids;
	}

	public void setFoodWalletMids(String foodWalletMids) {
		this.foodWalletMids = foodWalletMids;
	}

	public String getFoodWalletCategories() {
		return foodWalletCategories;
	}

	public void setFoodWalletCategories(String foodWalletCategories) {
		this.foodWalletCategories = foodWalletCategories;
	}

	public String getDailyTxnAmount() {
		return dailyTxnAmount;
	}

	public void setDailyTxnAmount(String dailyTxnAmount) {
		this.dailyTxnAmount = dailyTxnAmount;
	}

	public boolean isEditCall() {
		return editCall;
	}

	public void setEditCall(boolean editCall) {
		this.editCall = editCall;
	}

	public boolean isFoodWalletConfigured() {
		return foodWalletConfigured;
	}

	public void setFoodWalletConfigured(boolean foodWalletConfigured) {
		this.foodWalletConfigured = foodWalletConfigured;
	}

	public String getPanelLoginName() {
		return panelLoginName;
	}

	public void setPanelLoginName(String panelLoginName) {
		this.panelLoginName = panelLoginName;
	}
	public boolean getPrnValidation() {
		return prnValidation;
	}
	public void setPrnValidation(boolean prnValidation) {
		this.prnValidation = prnValidation;
	}
	public Integer getPrnExpiryTime() {
		return prnExpiryTime;
	}
	public void setPrnExpiryTime(Integer prnExpiryTime) {
		this.prnExpiryTime = prnExpiryTime;
	}
	public Integer getMaxPrnValidRetryAllowTime() {
		return maxPrnValidRetryAllowTime;
	}
	public void setMaxPrnValidRetryAllowTime(Integer maxPrnValidRetryAllowTime) {
		this.maxPrnValidRetryAllowTime = maxPrnValidRetryAllowTime;
	}
	public Integer getMaxPrnValidRetryAllowCount() {
		return maxPrnValidRetryAllowCount;
	}
	public void setMaxPrnValidRetryAllowCount(Integer maxPrnValidRetryAllowCount) {
		this.maxPrnValidRetryAllowCount = maxPrnValidRetryAllowCount;
	}
	public boolean isQrprn() {
		return qrprn;
	}
	public void setQrprn(boolean qrprn) {
		this.qrprn = qrprn;
	}

	public boolean isOnlineSettlement() {
		return onlineSettlement;
	}

	public void setOnlineSettlement(boolean onlineSettlement) {
		this.onlineSettlement = onlineSettlement;
	}

	public String getOnlineSettlementEnabled() {
		return onlineSettlementEnabled;
	}

	public void setOnlineSettlementEnabled(String onlineSettlementEnabled) {
		this.onlineSettlementEnabled = onlineSettlementEnabled;
	}
	public String getPrnValidationPref() {
		return prnValidationPref;
	}
	public void setPrnValidationPref(String prnValidationPref) {
		this.prnValidationPref = prnValidationPref;
	}
	public boolean isCancelAllowed() {
		return cancelAllowed;
	}

	public void setCancelAllowed(boolean cancelAllowed) {
		this.cancelAllowed = cancelAllowed;
	}

	public String getCancelAllowedEnabled() {
		return cancelAllowedEnabled;
	}

	public void setCancelAllowedEnabled(String cancelAllowedEnabled) {
		this.cancelAllowedEnabled = cancelAllowedEnabled;
	}

	public Integer getCancelAllowTime() {
		return cancelAllowTime;
	}

	public void setCancelAllowTime(Integer cancelAllowTime) {
		this.cancelAllowTime = cancelAllowTime;
	}

	public String getApiVersion() {
		return apiVersion;
	}
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
		
	public String getVan() {
		return van;
	}

	public void setVan(String van) {
		this.van = van;
	}
	public boolean isSdMerchant() {
		return sdMerchant;
	}
	public void setSdMerchant(boolean sdMerchant) {
		this.sdMerchant = sdMerchant;
	}

	public boolean isBwEnabled() {
		return bwEnabled;
	}

	public void setBwEnabled(boolean bwEnabled) {
		this.bwEnabled = bwEnabled;
	}

	public String getBwEnabledValue() {
		return bwEnabledValue;
	}

	public void setBwEnabledValue(String bwEnabledValue) {
		this.bwEnabledValue = bwEnabledValue;
	}

	public String getBwTransferMode() {
		return bwTransferMode;
	}

	public void setBwTransferMode(String bwTransferMode) {
		this.bwTransferMode = bwTransferMode;
	}

	public String getBwTriggerMode() {
		return bwTriggerMode;
	}

	public void setBwTriggerMode(String bwTriggerMode) {
		this.bwTriggerMode = bwTriggerMode;
	}

	public String getBwTriggerValue() {
		return bwTriggerValue;
	}

	public void setBwTriggerValue(String bwTriggerValue) {
		this.bwTriggerValue = bwTriggerValue;
	}


	public String getFlagMerchantValue() {
		return flagMerchantValue;
	}

	public void setFlagMerchantValue(String flagMerchantValue) {
		this.flagMerchantValue = flagMerchantValue;
	}	

	public boolean isBwAutoTransfer() {
		return bwAutoTransfer;
	}

	public void setBwAutoTransfer(boolean bwAutoTransfer) {
		this.bwAutoTransfer = bwAutoTransfer;
	}

	public String getPartialRenewalAllowedEnabled() {
		return partialRenewalAllowedEnabled;
	}
	public void setPartialRenewalAllowedEnabled(String partialRenewalAllowedEnabled) {
		this.partialRenewalAllowedEnabled = partialRenewalAllowedEnabled;
	}
	public boolean isPartialRenewalAllowed() {
		return partialRenewalAllowed;
	}
	public void setPartialRenewalAllowed(boolean partialRenewalAllowed) {
		this.partialRenewalAllowed = partialRenewalAllowed;
	}
	public Integer getMinPartialPercentage() {
		return minPartialPercentage;
	}
	public void setMinPartialPercentage(Integer minPartialPercentage) {
		this.minPartialPercentage = minPartialPercentage;
	}
	public boolean isS2sRefund() {
		return s2sRefund;
	}

	public void setS2sRefund(boolean s2sRefund) {
		this.s2sRefund = s2sRefund;
	}

	public String getS2sRefundEnabled() {
		return s2sRefundEnabled;
	}

	public void setS2sRefundEnabled(String s2sRefundEnabled) {
		this.s2sRefundEnabled = s2sRefundEnabled;
	}

	public String getS2sRefundUrl() {
		return s2sRefundUrl;
	}

	public void setS2sRefundUrl(String s2sRefundUrl) {
		this.s2sRefundUrl = s2sRefundUrl;
	}

	public boolean isS2SRefundUrlUpdated() {
		return isS2SRefundUrlUpdated;
	}

	public void setS2SRefundUrlUpdated(boolean isS2SRefundUrlUpdated) {
		this.isS2SRefundUrlUpdated = isS2SRefundUrlUpdated;
	}

	public String getFfPeonUrl() {
		return ffPeonUrl;
	}
	public void setFfPeonUrl(String ffPeonUrl) {
		this.ffPeonUrl = ffPeonUrl;
	}

	public boolean isFfPeonUrlUpdated() {
		return ffPeonUrlUpdated;
	}
	
	public void setFfPeonUrlUpdated(boolean ffPeonUrlUpdated) {
		this.ffPeonUrlUpdated = ffPeonUrlUpdated;
	}

	public String getAutoDebitEnabled() {
		return autoDebitEnabled;
	}
	public void setAutoDebitEnabled(String autoDebitEnabled) {
		this.autoDebitEnabled = autoDebitEnabled;
	}
	public boolean isAutoDebit() {
		return autoDebit;
	}
	public void setAutoDebit(boolean autoDebit) {
		this.autoDebit = autoDebit;
	}

	public String getAgreementContractNumber() {
		return agreementContractNumber;
	}

	public void setAgreementContractNumber(String agreementContractNumber) {
		this.agreementContractNumber = agreementContractNumber;
	}

	public Integer getAgreementConfirmTimeout() {
		return agreementConfirmTimeout;
	}

	public void setAgreementConfirmTimeout(Integer agreementConfirmTimeout) {
		this.agreementConfirmTimeout = agreementConfirmTimeout;
	}

	public String getUnlinkNotifyUrl() {
		return unlinkNotifyUrl;
	}

	public void setUnlinkNotifyUrl(String unlinkNotifyUrl) {
		this.unlinkNotifyUrl = unlinkNotifyUrl;
	}

	public Boolean getUnlinkNeedAgreement() {
		return unlinkNeedAgreement;
	}

	public void setUnlinkNeedAgreement(Boolean unlinkNeedAgreement) {
		this.unlinkNeedAgreement = unlinkNeedAgreement;
	}

	public Boolean getSupportInitOtp() {
		return supportInitOtp;
	}

	public void setSupportInitOtp(Boolean supportInitOtp) {
		this.supportInitOtp = supportInitOtp;
	}

	public Boolean getRaEnable() {
		return raEnable;
	}
	public void setRaEnable(Boolean raEnable) {
		this.raEnable = raEnable;
	}
	public Boolean getLobEnable() {
		return lobEnable;
	}
	public void setLobEnable(Boolean lobEnable) {
		this.lobEnable = lobEnable;
	}
	public String getRaEnabled() {
		return raEnabled;
	}
	public void setRaEnabled(String raEnabled) {
		this.raEnabled = raEnabled;
	}
	public String getLobEnabled() {
		return lobEnabled;
	}
	public void setLobEnabled(String lobEnabled) {
		this.lobEnabled = lobEnabled;
	}
	public Boolean getApiDisable() {
		return apiDisable;
	}
	public void setApiDisable(Boolean apiDisable) {
		this.apiDisable = apiDisable;
	}
	public String getApiDisabled() {
		return apiDisabled;
	}
	public void setApiDisabled(String apiDisabled) {
		this.apiDisabled = apiDisabled;
	}

	public boolean isEnableCashierPage() {
		return enableCashierPage;
	}

	public void setEnableCashierPage(boolean enableCashierPage) {
		this.enableCashierPage = enableCashierPage;
	}

	public String getEnableCashierPageEnabled() {
		return enableCashierPageEnabled;
	}

	public void setEnableCashierPageEnabled(String enableCashierPageEnabled) {
		this.enableCashierPageEnabled = enableCashierPageEnabled;
	}

	public String getMerchantType() {
		return merchantType;
	}
	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}
	public boolean isMockMerchant() {
		return mockMerchant;
	}
	public void setMockMerchant(boolean mockMerchant) {
		this.mockMerchant = mockMerchant;
	}
	public String getMockEnable() {
		return mockEnable;
	}
	public void setMockEnable(String mockEnable) {
		this.mockEnable = mockEnable;
	}
}
