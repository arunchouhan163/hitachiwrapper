package com.paytm.wrapper.hitachiwrapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class Main {

	public static void main(String args[]) throws Exception {
		System.out.println((new Main()).solve("ababa"));
	}

	public int solve(String A) {
		StringBuilder sb = new StringBuilder(A);
		Set<Integer> set = new HashSet<>();
		List<Integer> listOfMatches = new ArrayList<>();
		for (int i = 0; i < A.length(); ++i) {
			set.add(hash(sb,0,i+1));
			if (sb.charAt(i) == sb.charAt(0)) {
				listOfMatches.add(i);
			}
		}

		List<Integer> multiplications = listOfMatches.parallelStream().map(x -> returnMultiply(x,set,sb)).collect(java.util.stream.Collectors.toList());
		long product =1;
		for (Integer i : multiplications) {
			product =(product*i ) % (1000_000_000+7);
		}
		return (int)product % (1000_000_000+7);
	}

	public static Integer returnMultiply(Integer i,Set<Integer> set,StringBuilder sb) {
		long product =1;
		for(int j=i;j<sb.length();++j) {
			if(set.contains(hash(sb,i, j+1))) {
				product = (product * (j+1-i)) % (1000_000_000+7);
			}
		}
		return (int)product % (1000_000_000+7);
	}
	
	public static int hash(StringBuilder sb,int start,int end) {
		int hash = 7;
		for (int i = start; i < end; i++) {
		    hash = hash*31 + sb.charAt(i);
		}
		return hash;
	}


}
