package com.paytm.wrapper.hitachiwrapper;

import org.springframework.cloud.openfeign.FeignClient;

import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

@FeignClient(name="hitachi-service",configuration=Hitachiconfiguration.class, url="http://172.18.24.251:8080")
public interface HitachiServiceProxy {
	
	@RequestLine("POST /POSBoarding/Services/MerchantBording")
	@Headers("Content-Type: application/json")
	@Body("{body}")
	public String postToTerminal(@Param("body")String body);
	
	
}
