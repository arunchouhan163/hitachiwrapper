package com.paytm.wrapper.hitachiwrapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class HitachiwrapperApplication {

	public static void main(String[] args) {
		SpringApplication.run(HitachiwrapperApplication.class, args);
	}
}
