package com.paytm.wrapper.hitachiwrapper;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/v1")
public class HitachiController {

	public static boolean y = true;
	private Logger LOGGER = LoggerFactory.getLogger(HitachiController.class);
	@Autowired
	private HitachiServiceProxy hitachiService;
	@Autowired
	private LocalHit localhti;

	@PostMapping()
	public String onboardTerminal(@RequestBody String body) {
		LOGGER.info("Request mbody:{}", body);
		String response = hitachiService.postToTerminal(body);
		LOGGER.info("response : {}", response);
		return response;
	}

	@PostMapping("/dummy")
	// @PostMapping()
	public String onboardTerminal() {
		String response = "{\"Rsp_code\":\"30\"}";
		if (y) {
			response =
					"{\"S_no\":768,\"RetailerID\":\"jhknuhnjkj,n\",\"MID\":\"r5756guyutykh\",\"TID\":\"ksau7hv8ri\",\"BankName\":\"HITACHI\",\"Rsp_code\":\"00\"}";
		}
		y = !y;
		LOGGER.info("Dummy response : {}", response);
		return response;
	}

	@GetMapping("/try/mid/{mid}")
	public String getTerminal(@PathVariable("mid") String mid) {
		try {
			String client = generateToken();
			System.out.println(client);
			String x = localhti.getTerminal(client, mid);
			return x;
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			LOGGER.error("Error :{}", e.getResponseBodyAsString());
			e.printStackTrace();
			return e.getResponseBodyAsString();
		}
	}

	@GetMapping("/create/mid/{mid}/serial/{serialNo}/model/{modelNo}")
	public String createTerminal(@PathVariable("mid") String mid,@PathVariable("serialNo") String serialNo,@PathVariable("modelNo") String modelNo) {
		try {
			String client = generateToken();
			System.out.println(client);
			localhti.createTerminal(client,serialNo,mid,modelNo);
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			LOGGER.error("Error :{}", e.getResponseBodyAsString());
			return e.getResponseBodyAsString();
		}
		return null;
	}

	public static String generateToken() {
		String BO_KEY = "hhzuHzBlYMh7d/b1nyvZDHYy2vAp23FGqJsglpDpItrT9p7XN2BnPMRtkMu5lict0HnLPod9vxUPbVuem3yrgA==";
		String BO_CLIENT_ID = "8b6d61c5-3fb4-445b-bdca-83cbff08bb7d";
		byte[] decodedKey = Base64.getDecoder().decode(BO_KEY);

		Map<String, Object> claims = new HashMap<String, Object>();
		claims.put("client-id", BO_CLIENT_ID);

		String token = Jwts.builder().setIssuedAt(new Date()).addClaims(claims).signWith(SignatureAlgorithm.HS512, decodedKey).compact();
		return token;
	}
}
