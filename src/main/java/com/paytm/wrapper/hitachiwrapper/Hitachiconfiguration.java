package com.paytm.wrapper.hitachiwrapper;

import org.springframework.context.annotation.Bean;

import feign.Contract;
import feign.Logger;
import feign.codec.ErrorDecoder;

public class Hitachiconfiguration {
	
    @Bean
    public Contract feignContract() {
        return new feign.Contract.Default();
    }
    
    
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
    
    @Bean
    public ErrorDecoder errorDecoder() {
    	return new SpringWebClientErrorDecoder();
    }
}
